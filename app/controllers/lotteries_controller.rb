class LotteriesController < ApplicationController
  before_action :set_lottery, only: %i[ show edit update destroy ]
  before_action :set_lottery_time

  # GET /lotteries or /lotteries.json
  def index
    if !LotteryTopic.pluck(:title).include?(params[:lot])
      respond_to do |format|
        format.html { redirect_to root_path, alert: "ไม่มี Lottery ที่ท่านเลือก" }
      end
    end
    @lottery_topic = LotteryTopic.find_by(title: params[:lot])
    @lotteries = Lottery.all
    # @bills = current_user.bills.where(created_at: Date.today.all_day).reverse
    # @bills = current_user.bills.where(owner_id: current_user.id).where(created_at: Date.today.all_day, lottery_id: params[:lot]).order(created_at: :desc)
    @bills = current_user.bills.where(created_at: Date.today.all_day, lottery_id: params[:lot]).order(created_at: :desc)
    # @morning_bills = @bills.where(round: 1)
    # @noon_bills = @bills.where(round: 2)
    # @afternoon_bills = @bills.where(round: 3)
    # @evening_bills = @bills.where(round: 4)
  end

  # GET /lotteries/1 or /lotteries/1.json
  def show
  end

  # GET /lotteries/new
  def new
    @lottery_topic = LotteryTopic.find_by(title: params[:lot])
    @bill = current_user.bills.find(params[:bill_id])
    @lotteries = @bill.lotteries.reverse
    @lottery = Lottery.new
  end

  # GET /lotteries/1/edit
  def edit
  end

  # POST /lotteries or /lotteries.json
  def create

    # if @open_morning
    #   round = 1
    # elsif @close_noon
    #   round = 2
    # elsif @open_afternoon
    #   round = 3
    # elsif @close_evening
    #   round = 4
    # end

    bill = current_user.bills.find(params[:lottery][:bill])
    round = bill.round

    if params[:lottery][:price].present? || params[:lottery][:bottom_price].present? || params[:lottery][:reverse_price].present?

      ActiveRecord::Base.transaction do

        if params[:lottery][:two_digits]
          Lottery.two_digits(round, params, current_user, bill, params[:lottery][:reverse])
          respond_to do |format|
            bill.group = bill.group + 1
            bill.save
            format.html { redirect_to new_bill_lottery_url(bill, lot: bill.lottery_id), notice: "เพิ่มเลขที่ท่านต้องการบน setthaivip เรียบร้อยแล้ว" }
          end

        elsif params[:lottery][:rood]
          Lottery.rood_two_digits(round, params, current_user, bill)
          respond_to do |format|
            bill.group = bill.group + 1
            bill.save
            format.html { redirect_to new_bill_lottery_url(bill, lot: bill.lottery_id), notice: "เพิ่มเลขที่ท่านต้องการบน setthaivip เรียบร้อยแล้ว" }
          end

        elsif params[:lottery][:run]
          Lottery.run(round, params, current_user, bill)
          respond_to do |format|
            bill.group = bill.group + 1
            bill.save
            format.html { redirect_to new_bill_lottery_url(bill, lot: bill.lottery_id), notice: "เพิ่มเลขที่ท่านต้องการบน setthaivip เรียบร้อยแล้ว" }
          end
        elsif params[:lottery][:three]
          Lottery.three(round, params, current_user, bill, params[:lottery][:six_reverse])
          respond_to do |format|
            bill.group = bill.group + 1
            bill.save
            format.html { redirect_to new_bill_lottery_url(bill, lot: bill.lottery_id), notice: "เพิ่มเลขที่ท่านต้องการบน setthaivip เรียบร้อยแล้ว" }
          end
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to new_bill_lottery_url(bill, lot: bill.lottery_id), alert: "เพิ่ม Lottery ผิดพลาด กรุณาใส่จำนวนเงิน" }
      end
    end
  end

  # PATCH/PUT /lotteries/1 or /lotteries/1.json
  def update
    respond_to do |format|
      if @lottery.update(lottery_params)
        format.html { redirect_to lottery_url(@lottery), notice: "Lottery was successfully updated." }
        format.json { render :show, status: :ok, location: @lottery }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @lottery.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lotteries/1 or /lotteries/1.json
  def destroy
    @lottery.destroy

    @lottery.bill.update_attributes(
      total: @lottery.bill.total.to_i-@lottery.price.to_i,
      discount: sprintf('%.1f', @lottery.bill.discount-@lottery.discount)
    )

    respond_to do |format|
      format.html { redirect_to new_bill_lottery_url(@lottery.bill), notice: "ลบ #{ @lottery.datum} เรียบร้อย" }
      format.json { head :no_content }
    end
  end

  def rood()
    # lot = Lottery.rood(params[:data])
    lot = Lottery.rood(params[:data], params[:external])
    respond_to do |format|
      format.html
      format.json {render json: [Lottery.create_html(lot)]}
    end
  end

  def reverse()
    lot = Lottery.reverse(params[:data], params[:data_b])
    respond_to do |format|
      format.html
      format.json {render json: [Lottery.create_html(lot)]}
    end
  end

  def all_reverse()
    lot = Lottery.three_to_six(params[:data], params[:data_b])
    respond_to do |format|
      format.html
      format.json {render json: [Lottery.create_three_html(lot)]}
    end
  end

  def run()
    lot = Lottery.run_array(params[:data])
    respond_to do |format|
      format.html
      format.json {render json: [Lottery.create_run_html(lot)]}
    end
  end

  def destroy_group

    bill = Bill.find(params[:bill])
    lotteries = bill.lotteries.where(id: params[:lots])

    bill.update_attributes(
      total: bill.total.to_i-lotteries.map { |p| p.price.to_i }.sum,
      discount: sprintf('%.1f', bill.discount-lotteries.map { |p| p.discount.to_f }.sum),
      real_price: bill.real_price.to_i-lotteries.map { |p| p.real_price.to_i }.sum
    )
    
    if lotteries.delete_all
      respond_to do |format|
        format.html { redirect_to new_bill_lottery_url(bill, lot: bill.lottery_id), notice: "ลบ Lotteryเรียบร้อยแล้ว" }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lottery
      @lottery = Lottery.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def lottery_params
      params.require(:lottery).permit(:datum, :position, :price, :status, :name, :user_id)
    end

end
