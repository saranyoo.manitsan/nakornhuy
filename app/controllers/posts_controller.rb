class PostsController < ApplicationController
  before_action :set_post, only: %i[ show edit update destroy ]
  before_action :authenticate_user_role!
  before_action :set_lottery_time

  # GET /posts or /posts.json
  def index

    @lottery_topic = LotteryTopic.find_by(title: params[:lot])

    round = 1

    round = params[:search][:round][0] if params[:search].present?

    @bills = Bill.where(lottery_id: params[:lot], original_id: nil, :status => 'confirmed', round: round, created_at: Date.today.all_day)

    # @lotteries = Post.lottery_analyze(round, params[:lot])

    @summaries = Summary.where(
      lottery_name: params[:lot],
      round: round
    )

  end

  # GET /posts/1 or /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
    # @posts = Post.where(lottery_id: 'setthaivip')
    @lottery_topic = LotteryTopic.find_by(title: params[:lot])
    @posts = Post.where(lottery_id: params[:lot]).reverse
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts or /posts.json
  def create
    @post = Post.new(post_params)



    respond_to do |format|

      if @post.save
        round = @post.status

        Bill.where(lottery_id: @post.lottery_id, round:round, created_at: Date.today.all_day, total: [nil, "", 0]).delete_all
        Bill.where(lottery_id: @post.lottery_id, status: 'draft', round:round, created_at: Date.today.all_day).delete_all


        confirmed_lots = Lottery.joins(:bill).where(bills: {lottery_id: @post.lottery_id, status: 'confirmed', round:round, created_at: Date.today.all_day })
        passive_lots = Lottery.joins(:bill).where(bills: {lottery_id: @post.lottery_id, status: 'passive', round:round, created_at: Date.today.all_day })

        lose_lotteries = confirmed_lots.where(status: nil)

        lose_lotteries.each do |lot|
          lot.status = 'lose'
          lot.bill.status = 'passive'
          lot.save
          lot.bill.save
        end
        
        three_all_reverse_array = passive_lots.where(position: 5).where("datum like ?", "%#{params[:post][:three]}%").where("datum like ?", "%#{params[:post][:top].split('')[0]}%").where("datum like ?", "%#{params[:post][:top].split('')[1]}%").pluck(:datum).map {|p| p if p.split('').map(&:to_i).sum == params[:post][:three].to_i+params[:post][:top][0].to_i+params[:post][:top][1].to_i}.compact

        won_lotteries = passive_lots.where(datum: params[:post][:top], position: 1).or(passive_lots.where(datum: params[:post][:bottom], position: 2)).or(passive_lots.where(datum: params[:post][:three]+params[:post][:top], position: 4)).or(passive_lots.where(position: 5).where("datum like ?", "%#{params[:post][:three]}%").where("datum like ?", "%#{params[:post][:top].split('')[0]}%").where("datum like ?", "%#{params[:post][:top].split('')[1]}%").where(datum: three_all_reverse_array)).or(passive_lots.where("length(datum) = 1").where(position: 1).where("datum like ?", "%#{params[:post][:top].split('')[0]}%").or(passive_lots.where("length(datum) = 1").where(position: 1).where("datum like ?", "%#{params[:post][:top].split('')[1]}%")).or(passive_lots.where("length(datum) = 1").where(position: 1).where("datum like ?", "%#{params[:post][:three]}%"))).or(passive_lots.where("length(datum) = 1").where(position: 2).where("datum like ?", "%#{params[:post][:bottom].split('')[0]}%").or(passive_lots.where("length(datum) = 1").where(position: 2).where("datum like ?", "%#{params[:post][:bottom].split('')[1]}%")))

        won_lotteries.each do |lot|
          lot.status = 'won'
          lot.bill.status = 'active'
          lot.save
          lot.bill.amount_won = lot.bill.amount_won.to_f+lot.won_amount.to_f
          lot.bill.result_amount = lot.bill.result_amount.to_f+lot.result_amount.to_f
          lot.bill.fight_paid = lot.bill.fight_paid.to_f+lot.fight_paid.to_f
          lot.bill.save

          lot.bill.user.user_datum.credit = lot.bill.user.user_datum.credit + lot.bill.amount_won.to_f
          lot.bill.user.user_datum.save
        end

        users = User.joins(:bills).where(:bills => {round: round, created_at: Date.today.all_day}).uniq


        users.each do |user|

          all_bill = user.bills.where(lottery_id: @post.lottery_id, round: round, created_at: Date.today.all_day )
          @invoice = Invoice.new
          @invoice.user = user
          @invoice.lottery_id = @post.lottery_id
          @invoice.round = round
          @invoice.status = 'wait'
          @invoice.amount_won = user.bills.where( status: 'active', round: round, created_at: Date.today.all_day ).map { |p| p.amount_won.to_f }.sum
          @invoice.total = all_bill.map { |p| p.total.to_f }.sum 
          @invoice.discount = sprintf('%.1f', all_bill.map { |p| p.discount.to_f }.sum)

          @invoice.amount_total = sprintf('%.1f', @invoice.amount_won.to_f-(@invoice.total.to_f-@invoice.discount))

          @invoice.fight_paid = all_bill.map { |p| p.fight_paid.to_f }.sum 
          @invoice.real_price = all_bill.map { |p| p.real_price.to_f }.sum 
          @invoice.result_amount = all_bill.map { |p| p.result_amount.to_f }.sum 
          
          @invoice.save
          
          all_bill.each do |bill|
            bill.invoice = @invoice
            bill.amount_total = sprintf('%.1f', (bill.amount_won.to_f+bill.discount.to_f)-bill.total.to_f)
            bill.save
          end
        end

        round_invoices = Invoice.joins(:user).where(users: {parent_id: 1}).where(lottery_id: @post.lottery_id, round: round, created_at: Date.today.all_day)

        admin_invoice = Invoice.new
        admin_invoice.lottery_id = @post.lottery_id
        admin_invoice.user = User.find_by(role: 'admin')
        admin_invoice.round = round
        admin_invoice.status = 'wait'
        admin_invoice.amount_won = round_invoices.map { |p| p.amount_won.to_f }.sum
        admin_invoice.total = round_invoices.map { |p| p.total.to_f }.sum 
        admin_invoice.discount = sprintf('%.1f', round_invoices.map { |p| p.discount.to_f }.sum)
        admin_invoice.amount_total = sprintf('%.1f', admin_invoice.amount_won.to_f-(admin_invoice.total.to_f-admin_invoice.discount))
        admin_invoice.save

        @summaries = Summary.where(
          lottery_name: @post.lottery_id,
          round: round,
          created_at: Date.today.all_day
        ).delete_all

        format.html { redirect_to post_url(@post), notice: "Post was successfully created." }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1 or /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to post_url(@post), notice: "Post was successfully updated." }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1 or /posts/1.json
  def destroy
    @post.destroy

    respond_to do |format|
      format.html { redirect_to posts_url, notice: "Post was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def post_params
      params.require(:post).permit(:top, :bottom, :three, :status, :note, :lottery_id)
    end

    def authenticate_user_role!
      lot = params[:lot]
      if params[:post].present?
        lot = params[:post][:lottery_id]
      end
      unless current_user.admin? || (current_user.lottery_owner.include?(lot))
        redirect_to root_path
      end
    end

    def set_lottery_time
      @open_morning = DateTime.now.between?(Time.zone.parse("05:00"), Time.zone.parse("08:50"))
      @close_noon = DateTime.now.between?(Time.zone.parse("8:50"), Time.zone.parse("12:05"))
      # @open_afternoon = DateTime.now.between?(Time.zone.parse("12:05"), Time.zone.parse("14:55"))
      @close_evening = DateTime.now.between?(Time.zone.parse("15:10"), Time.zone.parse("17:10"))
      @open_afternoon = true
    end
end
