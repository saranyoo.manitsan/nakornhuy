class AgentsController < ApplicationController

  before_action :set_agent, only: [:show, :edit]
  before_action :authenticate_user_role!

  def index
    if current_user.super_admin?
      @agents = User.where(role: 'agent')
    else
      @agents = User.where(parent_id: current_user.id, role: 'agent')
    end
  end

  def new
    @agent = Agent.new
  end

  def create
    @user = User.new(agent_params)
    @user.role = 'agent'
    @user.parent_id = current_user.id
    @user.password = SecureRandom.hex(4)
    @user.password_confirmation = @user.password

    respond_to do |format|
      if @user.save
        format.html { redirect_to agents_url, notice: "เพิ่ม Agent แล้ว" }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_agent
      @user = User.find(params[:id])
    end

    def agent_params
      params.require(:agent).permit(:email, :name, :role, :password, :password_confirmation, :username)
    end

    def authenticate_user_role!
      unless current_user.avalability_agent?
        redirect_to root_path
      end
    end
end
