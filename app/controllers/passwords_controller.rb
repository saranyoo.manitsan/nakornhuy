class PasswordsController < ApplicationController

  def show
    raw, enc = Devise.token_generator.generate(User, :reset_password_token)
    current_user.update_columns(reset_password_token: enc, reset_password_sent_at: Time.current)

    if  !current_user.reset_password_period_valid?
        flash[:alert] = "เปลี่ยนรหัสผ่านผิดพลาด" 
        redirect_to profile_path(current_user)
        return
    end
  end

  def update


    if params[:user][:password] != params[:user][:password_confirmation]
      flash[:alert] = "รหัสผ่านใหม่และยืนยันรหัสผ่านไม่เหมือนกัน" 
      redirect_back(fallback_location: root_path)
      return
    end

    if current_user.reset_password(params[:user][:password],params[:user][:password_confirmation])
      current_user.save
      sign_in current_user, :bypass => true
      respond_to do |format|
        format.html { redirect_to profile_path(current_user), notice: 'เปลี่ยนรหัสผ่านเรียบร้อยแล้ว' }
      end
      else
      flash[:alert] = "เปลี่ยนรหัสผ่านผิดพลาด" 
      redirect_back(fallback_location: root_path)
    end
  end
end