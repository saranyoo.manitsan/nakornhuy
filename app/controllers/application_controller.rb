class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    attributes = [:name, :role, :parent_id, :username, :password, :password_confirmation]
    devise_parameter_sanitizer.permit(:sign_up, keys: attributes)
    devise_parameter_sanitizer.permit(:account_update, keys: attributes)
  end

  def set_lottery_time
    # @open_morning = DateTime.now.between?(Time.zone.parse("05:00"), Time.zone.parse("08:40"))
    # @close_noon = DateTime.now.between?(Time.zone.parse("8:50"), Time.zone.parse("11:55"))
    # @open_afternoon = DateTime.now.between?(Time.zone.parse("12:05"), Time.zone.parse("14:40"))
    # @close_evening = DateTime.now.between?(Time.zone.parse("14:50"), Time.zone.parse("16:55"))
    @open_afternoon = true
    # @open_morning = false
    # @close_noon = false
    # @open_afternoon = false
    # @close_evening = false
  end
end
