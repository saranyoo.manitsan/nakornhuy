class MemberInvoicesController < ApplicationController
  def index

    @invoices = Invoice.joins(:user).where(:users => { :id => User.where(parent_id: current_user.id).pluck(:id) }).order('created_at DESC')
    if params[:search].present?

      if params[:search][:lot].present?
        @invoices = @invoices.where(lottery_id: params[:search][:lot])
      end

      if params[:search][:status].present?
        @invoices = @invoices.where(status: params[:search][:status])
      end

      if params[:search][:user].present?
        @invoices = @invoices.where(:users => { :name => params[:search][:user] })
      end

      if params[:search][:start_date].present? && params[:search][:end_date].present?
        start_date = params[:search][:start_date].to_date.beginning_of_day
        end_date = params[:search][:end_date].to_date.end_of_day
        @invoices = @invoices.where(:created_at => start_date..end_date)
      end

    end

    @user = User.where(parent_id: current_user.id).pluck(:name)

    @invoices
  end

  def confirmed
    @invoice = Invoice.joins(:user).where(:users => { :id => User.where(parent_id: current_user.id) }).find(params[:member_id])
    @invoice.status = 'paid'
    respond_to do |format|
      if @invoice.save
        format.html { redirect_to agent_member_invoices_path(current_user), notice: "ยืนยันใบแจ้งยอดเรียบร้อยแล้ว" }
        format.json { render :show, status: :created, location: @invoice }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @invoice = Invoice.joins(:user).where(:users => { :id => User.where(parent_id: current_user.id).pluck(:id) }).find(params[:id])
  end
end
