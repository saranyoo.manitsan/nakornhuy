class BillsController < ApplicationController

  before_action :set_bill, only: %i[ show edit update destroy ]
  before_action :set_lottery_time

  def index
    @bills = current_user.bills.paginate(page: params[:page], per_page: 30).order('created_at DESC')
    # @bills_1 = Bill.where(created_at: Date.today.all_day, round:1)
    # @bills_2 = Bill.where(created_at: Date.today.all_day, round:2)
    # @bills_3 = Bill.where(created_at: Date.today.all_day, round:3)
    # @bills_4 = Bill.where(created_at: Date.today.all_day, round:4)
  end

  def new
    @lottery_topic = LotteryTopic.find_by(title: params[:lot])
    @bill = Bill.new
  end

  def create

    @lottery_topic = LotteryTopic.find_by(title: params[:bill][:lottery_id])

    if @lottery_topic.open_round?

      round = LotteryTime.set_round(@lottery_topic.lottery_times, @lottery_topic.lottery_dates)

      @bill = Bill.new(bill_params)
      
      if @bill.name.blank?
        @bill.name = 'ไม่มีชื่อ'
      end
      
      @bill.user = current_user
      @bill.status = 'draft'
      @bill.round = round
      @bill.lottery_id = params[:bill][:lottery_id]
      @bill.owner_id = current_user.id

      respond_to do |format|
        if @bill.save
          format.html { redirect_to new_bill_lottery_url(@bill, lot: @bill.lottery_id), notice: "สร้างบิลใหม่แล้ว" }
          format.json { render :show, status: :created, location: @bill }
        else
          format.html { render :new, status: :unprocessable_entity }
          format.json { render json: @bill.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to lotteries_url(lot: bill.lottery_id), alert: "หมดเวลาแล้ว" }
      end
    end
  end

  def confirmed
    @bill = Bill.find(params[:bill_id])

    @lottery_topic = LotteryTopic.find_by(title: @bill.lottery_id)

    if @lottery_topic.open_round?

      @bill.status = 'confirmed'
      respond_to do |format|
        if current_user.user_datum.credit > @bill.total.to_i
          if @bill.save

            current_user.user_datum.credit = current_user.user_datum.credit - @bill.total.to_i
            current_user.save


            parent_user = User.find(@bill.user.parent_id)

            if parent_user.role == 'agent'
              ActiveRecord::Base.transaction do
                agent_bill = @bill.dup
                agent_bill.user = parent_user
                agent_bill.original_id = @bill.id
                agent_bill.total = 0
                agent_bill.discount = 0
                agent_bill.real_price = 0
                agent_bill.fight_amount = 0
                agent_bill.save
                @bill.lotteries.each do |lot|

                  if (lot.position == "1" && lot.datum.length == 2)
                    params = {
                      lottery: {
                        datum: lot.datum,
                        price: lot.price
                      }
                    }
                    Lottery.two_digits_top(agent_bill.round, params, agent_bill.user, agent_bill, lot.position, nil)
                  end
                  if (lot.position == "2" && lot.datum.length == 2)
                    params = {
                      lottery: {
                        datum: lot.datum,
                        bottom_price: lot.price
                      }
                    }
                    Lottery.two_digits_bottom(agent_bill.round, params, agent_bill.user, agent_bill, lot.position, nil)
                  end
                  if (lot.position == "1" && lot.datum.length == 1)
                    params = {
                      lottery: {
                        datum: lot.datum,
                        price: lot.price
                      }
                    }
                    Lottery.run_top(agent_bill.round, params, agent_bill.user, agent_bill, lot.position)
                  end
                  if (lot.position == "2" && lot.datum.length == 1)
                    params = {
                      lottery: {
                        datum: lot.datum,
                        bottom_price: lot.price
                      }
                    }
                    Lottery.run_bottom(agent_bill.round, params, agent_bill.user, agent_bill, lot.position)
                  end
                  if (lot.position == "4")
                    params = {
                      lottery: {
                        datum: lot.datum,
                        price: lot.price
                      }
                    }
                    Lottery.three(agent_bill.round, params, agent_bill.user, agent_bill, nil)
                  end
                  if (lot.position == "5")
                    params = {
                      lottery: {
                        datum: lot.datum,
                        reverse_price: lot.price
                      }
                    }
                    Lottery.three_all_reverse(agent_bill.round, params, agent_bill.user, agent_bill, nil)
                  end
                end
              end
            end

            @bill.lotteries.each do |lot|
              sum = Summary.find_by(
                lottery_name: @bill.lottery_id,
                datum: lot.datum,
                position: lot.position,
                round: @bill.round
              )

              if sum.present?
                sum.total_purchaser = sum.total_purchaser.to_f + lot.real_price.to_f
                sum.position =  lot.position
                sum.round = @bill.round
                sum.number_purchaser = sum.number_purchaser.to_f + 1
                sum.save
              else
                Summary.create(
                  lottery_name: @bill.lottery_id,
                  datum: lot.datum,
                  total_purchaser: lot.real_price.to_f,
                  position: lot.position,
                  round: @bill.round,
                  number_purchaser: 1
                )
              end
            end


            # sum_lot = Summary.find_by(lottery_name: @bill.lottery_id)
            # unless sum_lot.present?
            #   sum_lot = Summary.create(
            #     lottery_name: @bill.lottery_id,
            #     round: @bill.round
            #   )

            #   @bill.lotteries.each do |lot|
            #     sum = sum_lot.find_by(datum: lot)
            #     if sum.present?
            #       sum.total_purchaser = sum.total_purchaser + lot.price
            #       sum.save
            #     else

            #     end
            #   end
            # else
            #   sum_lot
            # end


            format.html { redirect_to lotteries_url(lot: @bill.lottery_id), notice: "ยืนยันบิลของ #{@bill.name} เรียบร้อยแล้ว" }
            format.json { render :show, status: :created, location: @bill }
          else
            format.html { render :new, status: :unprocessable_entity }
            format.json { render json: @bill.errors, status: :unprocessable_entity }
          end
        else
          format.html { redirect_to lotteries_url(lot: @bill.lottery_id), alert: "ยอดเงินของคุณไม่เพียงพอ" }
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to lotteries_url(lot: @bill.lottery_id), alert: "หมดเวลาแล้ว" }
      end
    end
  end
 
  def destroy
    @lottery_topic = LotteryTopic.find_by(title: @bill.lottery_id)

    if @lottery_topic.open_round?

      parent_bill = Bill.find_by(original_id: @bill.id)
      parent_bill.destroy if parent_bill.present?

      @bill.lotteries.each do |lot|
        sum = Summary.find_by(
          lottery_name: @bill.lottery_id, 
          datum: lot.datum,
          position: lot.position,
          round: @bill.round
        )

        if sum.present?
          sum.total_purchaser = sum.total_purchaser.to_f - lot.real_price.to_f
          sum.position =  lot.position
          sum.round = @bill.round
          sum.number_purchaser = sum.number_purchaser.to_f - 1
          sum.save
        end
       
      end

      @bill.destroy

      respond_to do |format|
        format.html { redirect_to lotteries_url(lot: @bill.lottery_id), notice: "ลบบิลของ #{@bill.name} เรียบร้อยแล้ว" }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to lotteries_url(lot: @bill.lottery_id), alert: "หมดเวลาแล้ว" }
      end
    end
  end

  private
    def bill_params
      params.require(:bill).permit(:name, :round, :status)
    end

    def set_bill
      @bill = Bill.find(params[:id])
    end
end
