class ProfilesController < ApplicationController
  
  before_action :check_owner_user

  def edit
  end

  def show
    @parent = User.find(current_user.parent_id)
  end

  def update
    respond_to do |format|
      if current_user.update(profile_params)
        format.html { redirect_to profile_url(current_user), notice: "แก้ไขชื่อเรียบร้อยแล้ว" }
        format.json { render :show, status: :ok, location: current_user }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def check_owner_user
    redirect_to profile_path(current_user) if params[:id].to_i != current_user.id
  end

  def profile_params
    params.require(:user).permit(
      :name,
      user_datum_attributes:[
        :fight_percent,
        :fight_three,
        :fight_run,
        :two_fight_limit,
        :three_fight_limit,
        :run_fight_limit,
        :id
      ]
    )
  end
end
