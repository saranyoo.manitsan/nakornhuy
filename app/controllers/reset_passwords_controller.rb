class ResetPasswordsController < ApplicationController
  def show
    @user = User.where(parent_id: current_user.id).find(params[:user_id])
  end

  def update

    @user = User.where(parent_id: current_user.id).find(params[:user_id])

    if params[:user][:password] != params[:user][:password_confirmation]
      flash[:alert] = "รหัสผ่านใหม่และยืนยันรหัสผ่านไม่เหมือนกัน" 
      redirect_back(fallback_location: root_path)
      return
    end

    @user.reset_password(params[:user][:password],params[:user][:password_confirmation])

    if @user.save
      respond_to do |format|
        format.html { redirect_to user_path(@user), notice: 'เปลี่ยนรหัสผ่านเรียบร้อยแล้ว' }
      end
    else
      flash[:alert] = "เปลี่ยนรหัสผ่านผิดพลาด" 
      redirect_back(fallback_location: root_path)
    end
  end
end
