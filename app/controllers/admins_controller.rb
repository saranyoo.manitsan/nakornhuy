class AdminsController < ApplicationController

  before_action :set_admin, only: [:show, :edit]
  before_action :authenticate_user_role!

  def index
    @admins = User.where(role: 'admin')
  end

  def new
    @admin = Admin.new
  end

  def create
    @user = User.new(admin_params)
    @user.role = 'admin'
    @user.parent_id = current_user.id
    @user.password = SecureRandom.hex(4)
    @user.password_confirmation = @user.password

    respond_to do |format|
      if @user.save
        format.html { redirect_to admins_url, notice: "เพิ่ม ADMIN แล้ว" }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    
  end

  def update
    
  end

  private
    def set_admin
      @user = User.find(params[:id])
    end

    def admin_params
      params.require(:admin).permit(:email, :name, :role, :password, :password_confirmation, :username)
    end

    def authenticate_user_role!
      unless current_user.avalability_admin?
        redirect_to root_path
      end
    end
end
