class InvoicesController < ApplicationController

  def index
    @invoices = current_user.invoices
    if params[:search].present?
      if params[:search][:lot].present?
        @invoices = current_user.invoices.where(lottery_id: params[:search][:lot])
      else
        @invoices = current_user.invoices
      end

      if params[:search][:status].present?
        @invoices = @invoices.where(status: params[:search][:status])
      end

      if params[:search][:start_date].present? && params[:search][:end_date].present?
        start_date = params[:search][:start_date].to_date.beginning_of_day
        end_date = params[:search][:end_date].to_date.end_of_day
        @invoices = @invoices.where(:created_at => start_date..end_date)
      end
    end

    @invoices = @invoices.paginate(page: params[:page], per_page: 30).order('created_at DESC')
  end

  def show
    @invoice = current_user.invoices.find(params[:id])
  end
end
