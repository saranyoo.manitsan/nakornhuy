class LotteryTopicsController < ApplicationController
  before_action :set_lottery_topic, only: %i[ show edit update destroy ]
  before_action :authenticate_user_role!

  # GET /lottery_topics or /lottery_topics.json
  def index
    @lottery_topics = LotteryTopic.all.order('created_at ASC')
  end

  # GET /lottery_topics/1 or /lottery_topics/1.json
  def show
  end

  # GET /lottery_topics/new
  def new
    @lottery_topic = LotteryTopic.new
  end

  # GET /lottery_topics/1/edit
  def edit
  end

  # POST /lottery_topics or /lottery_topics.json
  def create
    @lottery_topic = LotteryTopic.new(lottery_topic_params)

    respond_to do |format|
      if @lottery_topic.save
        format.html { redirect_to lottery_topic_url(@lottery_topic), notice: "Lottery topic was successfully created." }
        format.json { render :show, status: :created, location: @lottery_topic }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @lottery_topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lottery_topics/1 or /lottery_topics/1.json
  def update
    respond_to do |format|
      if @lottery_topic.update(lottery_topic_params)
        format.html { redirect_to lottery_topic_url(@lottery_topic), notice: "Lottery topic was successfully updated." }
        format.json { render :show, status: :ok, location: @lottery_topic }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @lottery_topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lottery_topics/1 or /lottery_topics/1.json
  def destroy
    @lottery_topic.destroy

    respond_to do |format|
      format.html { redirect_to lottery_topics_url, notice: "Lottery topic was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lottery_topic
      @lottery_topic = LotteryTopic.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def lottery_topic_params
      params.require(:lottery_topic).permit(
        :name, 
        :title,
        :description, 
        :avatar,
        :close,
        lottery_times_attributes:[:start_time, :end_time, :round_name, :admin_end_time, :id, :_destroy],
        lottery_dates_attributes:[:day, :id, :_destroy]
      )
    end

    def authenticate_user_role!
      unless current_user.admin?
        redirect_to root_path
      end
    end
end
