class UsersController < ApplicationController

  # before_action :set_user, except: [:index, :create, :new]
  before_action :authenticate_user_role!

  def index
    # @users = User.all
    @users = User.where(parent_id: current_user.id)
    @members = User.where(parent_id: current_user.id, role: 'member')
    @agents = User.where(parent_id: current_user.id, role: 'agent')
  end

  def show
    @user = User.where(parent_id: current_user.id).find(params[:id])
  end

  def new
    @user = User.new
    @user_datum = @user.build_user_datum
  end

  def edit
    @user = User.where(parent_id: current_user.id).find(params[:id])
    @user_datum = @user.user_datum
  end

  def update
    @user = User.where(parent_id: current_user.id).find(params[:id])
    respond_to do |format|
      if @user.update(user_params)
        # format.html { redirect_to user_url(@user), notice: "แก้ไขผู้ใช้งานเรียบร้อยแล้ว" } if current_user.id.to_s == params[:id]
        # if current_user.id.to_s != params[:id]
        #   if @user.agent?
        #     format.html { redirect_to agents_url, notice: "แก้ไขผู้ใช้งานเรียบร้อยแล้ว" }
        #   end
        #   format.html { redirect_to members_url, notice: "แก้ไขผู้ใช้งานเรียบร้อยแล้ว" }
        # end
        if params[:id].to_i == current_user.id
          format.html { redirect_to profile_url(@user), notice: "แก้ไขผู้ใช้งานเรียบร้อยแล้ว" }
        else
          format.html { redirect_to users_url, notice: "แก้ไขผู้ใช้งานเรียบร้อยแล้ว" }
        end
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @user = User.new(user_params)
    @user.parent_id = current_user.id

    respond_to do |format|
      if @user.save
        # User.create_user_datum(@user, params)
        format.html { redirect_to users_url, notice: "เพิ่ม User แล้ว" }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy

    @user = User.where(parent_id: current_user.id).find(params[:id])

    @user.destroy

    respond_to do |format|
      format.html { redirect_to members_url, notice: "ลบ Member ชื่อ #{@user.name} แล้ว" }
      format.json { head :no_content }
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, 
        :role, 
        :email, 
        :username, 
        :password, 
        :password_confirmation,
        user_datum_attributes: [:credit, :percent, :three_percent, :three_reverse_percent, :run_percent, :run_user_percent, :three_user_percent, :run_bottom_percent, :user_percent, :id, :fightable ])
    end

    def authenticate_user_role!
      if current_user.member?
        redirect_to root_path
      end
    end
end
