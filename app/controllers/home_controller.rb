class HomeController < ApplicationController
  before_action :set_lottery_time
  
  def index
    @lottery_topics = LotteryTopic.all.order('created_at ASC')
  end

  def owner_lottery
    @lottery_topics = LotteryTopic.where(title: current_user.lottery_owner).order('created_at ASC')
  end
end
