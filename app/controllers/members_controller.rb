class MembersController < ApplicationController

  # before_action :authenticate_user_role!

  def index
    if current_user.super_admin?
      @members = User.where(role: 'member')
    else
      @members = User.where(parent_id: current_user.id, role: 'member')
    end
  end

  def edit
    
  end

  def new
    @member = User.new
    @user_datum = @member.build_user_datum
  end

  def create
    @user = User.new(member_params)
    @user.role = 'member'
    @user.parent_id = current_user.id
    respond_to do |format|
      if @user.save
        # User.create_user_datum(@user, params)
        format.html { redirect_to members_url, notice: "เพิ่ม Member แล้ว" }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_member
      @user = User.find(params[:id])
    end

    def member_params
      params.require(:member).permit(:email, :name, :role, :password, :password_confirmation, :username)
    end

    def authenticate_user_role!
      unless current_user.avalability_member?
        redirect_to root_path
      end
    end

end
