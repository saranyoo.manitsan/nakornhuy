$(document).on('ready turbolinks:load', function() {

  $('#profile #lottery_datum').on('input', function(e) {
      var value = $(".two_digits").val();
      if ($("#lottery_reverse").is(':checked')) {
        var value_b = $("#lottery_reverse").val();
      } else {
        var value_b = 0;
      }
      $.ajax({
        url:"/reverse",
        type:'GET',
        data: {data: value, data_b: value_b},
        dataType:"json",
        success:function(data){
          //update user on success/failure
          $("#two_digit_numbers").html(data[0].html.toString());
          if (data[0].lock) {
            $('input[type="submit"]').prop('disabled', true);
          } else {
            $('input[type="submit"]').prop('disabled', false);
          }
        }
      });
   
  })

  $('#home #lottery_datum').on('input', function(e) {
    var value = $("#home #lottery_datum").val();
    var option = $("#home #lottery_option:checked").val();

    if (value != '') {
      $.ajax({
        url:"/rood",
        type:'GET',
        data: {data: value, external: option},
        dataType:"json",
        success:function(data){
          //update user on success/failure
          $("#rood_number").html(data[0].html.toString());
          if (data[0].lock) {
            $('input[type="submit"]').prop('disabled', true);
          } else {
            $('input[type="submit"]').prop('disabled', false);
          }
        }
      });
    } else {
      $("#rood_number").html("");
    }
  })

  $('#home #lottery_option').change(function(e) {
    var value = $("#lottery_datum").val();
    $.ajax({
      url:"/rood",
      type:'GET',
      data: {data: value, external: this.value},
      dataType:"json",
      success:function(data){
        //update user on success/failure
        $("#rood_number").html(data[0].html.toString());
        if (data[0].lock) {
          $('input[type="submit"]').prop('disabled', true);
        } else {
          $('input[type="submit"]').prop('disabled', false);
        }
      }
    });
  })

  $('#lottery_reverse').change(function(e) {
    if ($("#" + e.target.getAttribute("id")).prop("checked") === true) {
      var value = $(".two_digits").val();
      var value_b = $("#lottery_reverse").val();
      $.ajax({
        url:"/reverse",
        type:'GET',
        data: {data: value, data_b: value_b},
        dataType:"json",
        success:function(data){
          //update user on success/failure
          $("#two_digit_numbers").html(data[0].html.toString());
          if (data[0].lock) {
            $('input[type="submit"]').prop('disabled', true);
          } else {
            $('input[type="submit"]').prop('disabled', false);
          }
        }
      });
    } else {
      $("#two_digit_numbers").html("");
    }

  })

  $('#messages #lottery_datum').on('input', function(e) {
    var value = $("#messages #lottery_datum").val();
    if ($("#lottery_six_reverse").is(':checked')) {
      var value_b = $("#lottery_six_reverse").val();
    } else {
      var value_b = 0;
    }
    $.ajax({
      url:"/all_reverse",
      type:'GET',
      data: {data: value, data_b: value_b},
      dataType:"json",
      success:function(data){
        //update user on success/failure
        $("#three_digit_numbers").html(data[0].html.toString());
        if (data[0].lock) {
          $('input[type="submit"]').prop('disabled', true);
        } else {
          $('input[type="submit"]').prop('disabled', false);
        }
      }
    });
  })



  $('#lottery_six_reverse').change(function(e) {
    if ($("#" + e.target.getAttribute("id")).prop("checked") === true) {
      var value = $(".three_digits").val();
      var value_b = $("#lottery_six_reverse").val();
      $.ajax({
        url:"/all_reverse",
        type:'GET',
        data: {data: value, data_b: value_b},
        dataType:"json",
        success:function(data){
          //update user on success/failure
          $("#three_digit_numbers").html(data[0].html.toString());
          if (data[0].lock) {
            $('input[type="submit"]').prop('disabled', true);
          } else {
            $('input[type="submit"]').prop('disabled', false);
          }
        }
      });
    } else {
      $("#three_digit_numbers").html("");
    }
  })

  $('#settings #lottery_datum').on('input', function(e) {
    var value = $("#settings #lottery_datum").val();
    
    $.ajax({
      url:"/run",
      type:'GET',
      data: {data: value},
      dataType:"json",
      success:function(data){
        //update user on success/failure
        $("#run_numbers").html(data[0].html.toString());
        if (data[0].lock) {
          $('input[type="submit"]').prop('disabled', true);
        } else {
          $('input[type="submit"]').prop('disabled', false);
        }
      }
    });
  })

  $('#new_lot a').click(function (e) {
    e.preventDefault()

    $('#new-lot1')[0].reset();
    $('#new-lot2')[0].reset();
    $('#new-lot3')[0].reset();
    $('#new-lot4')[0].reset();
    $("#rood_number").html("");
    $("#two_digit_numbers").html("");
    $("#three_digit_numbers").html("");
    $("#run_numbers").html("");
    $('input[type="submit"]').prop('disabled', false);
  })
});