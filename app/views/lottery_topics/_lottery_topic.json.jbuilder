json.extract! lottery_topic, :id, :name, :title, :created_at, :updated_at
json.url lottery_topic_url(lottery_topic, format: :json)
