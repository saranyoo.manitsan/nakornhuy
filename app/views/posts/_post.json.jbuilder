json.extract! post, :id, :top, :bottom, :three, :status, :note, :created_at, :updated_at
json.url post_url(post, format: :json)
