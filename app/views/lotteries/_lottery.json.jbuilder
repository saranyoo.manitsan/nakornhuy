json.extract! lottery, :id, :datum, :position, :price, :status, :name, :user_id, :created_at, :updated_at
json.url lottery_url(lottery, format: :json)
