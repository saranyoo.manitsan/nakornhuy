class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_one :user_datum
  has_many :lotteries
  has_many :bills
  has_many :invoices

  accepts_nested_attributes_for :user_datum

  attr_writer :login

  ROLES = ["agent", "member"]
  MEMBERROLES = ["member"]
  FIGHTABLE = [100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0]

  validates :name, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }
  validates_uniqueness_of :name, message: "ชื่อนี้ถูกใช้ไปแล้ว กรุณาลองชื่อใหม่" 
  validates :username, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }
  validates_uniqueness_of :username, message: "username นี้ถูกใช้ไปแล้ว กรุณาลอง username ใหม่"
  validates :email, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }

  def fightable

    self.user_datum.fightable = 0 if self.user_datum.fightable.nil?

    if self.user_datum.fightable == 100
      fightable = [100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0]
    elsif self.user_datum.fightable == 90
      fightable = [90, 80, 70, 60, 50, 40, 30, 20, 10, 0]
    elsif self.user_datum.fightable == 80
      fightable = [80, 70, 60, 50, 40, 30, 20, 10, 0]
    elsif self.user_datum.fightable == 70
      fightable = [70, 60, 50, 40, 30, 20, 10, 0]
    elsif self.user_datum.fightable == 60
      fightable = [60, 50, 40, 30, 20, 10, 0]
    elsif self.user_datum.fightable == 50
      fightable = [50, 40, 30, 20, 10, 0]
    elsif self.user_datum.fightable == 40
      fightable = [40, 30, 20, 10, 0]
    elsif self.user_datum.fightable == 30
      fightable = [30, 20, 10, 0]
    elsif self.user_datum.fightable == 20
      fightable = [20, 10, 0]
    elsif self.user_datum.fightable == 10
      fightable = [10, 0]
    elsif self.user_datum.fightable == 0
      fightable = [0]
    end
    fightable
  end

  def login
    @login || self.username || self.email
  end

  def super_admin?
    self.role == "super_admin"
  end

  def admin?
    self.role == "admin"
  end

  def agent?
    self.role == "agent"
  end

  def member?
    self.role == "member"
  end

  def avalability_member?
    self.super_admin? || self.admin? || self.agent?
  end

  def avalability_agent?
    self.super_admin? || self.admin?
  end

  def avalability_admin?
    self.super_admin?
  end

  def self.create_user_datum(user, params)
    UserDatum.create(
      user: user,
      percent: params[:member][:percent],
      user_percent: params[:member][:user_percent],
      three_percent: params[:member][:three_percent],
      three_reverse_percent: params[:member][:three_reverse_percent],
      run_percent: params[:member][:run_percent],
    )
  end
end
