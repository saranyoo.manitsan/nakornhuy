class UserDatum < ApplicationRecord
  belongs_to :user

  validates :percent, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }
  validates_numericality_of :percent, message: "ใส่ได้เพียงตัวเลข"
  validates :user_percent, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }
  validates_numericality_of :user_percent, message: "ใส่ได้เพียงตัวเลข"
  validates :three_percent, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }
  validates_numericality_of :three_percent, message: "ใส่ได้เพียงตัวเลข"
  validates :three_reverse_percent, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }
  validates_numericality_of :three_reverse_percent, message: "ใส่ได้เพียงตัวเลข"
  validates :run_percent, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }
  validates_numericality_of :run_percent, message: "ใส่ได้เพียงตัวเลข"
  validates :run_bottom_percent, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }
  validates_numericality_of :run_bottom_percent, message: "ใส่ได้เพียงตัวเลข"

  validates_numericality_of :run_fight_limit, message: "ใส่ได้เพียงตัวเลข"
  validates :run_fight_limit, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }

  validates_numericality_of :three_fight_limit, message: "ใส่ได้เพียงตัวเลข"
  validates :three_fight_limit, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }

  validates_numericality_of :two_fight_limit, message: "ใส่ได้เพียงตัวเลข"
  validates :two_fight_limit, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }
end
