class LotteryTopic < ApplicationRecord
  has_many :lottery_times
  has_many :lottery_dates
  
  mount_uploader :avatar, AvatarUploader

  accepts_nested_attributes_for :lottery_times, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :lottery_dates, reject_if: :all_blank, allow_destroy: true

  def open_round?
    LotteryTime.set_date_time(self.lottery_times, self.lottery_dates).class == String
  end

  def round_name
    LotteryTime.set_date_time(self.lottery_times, self.lottery_dates)
  end

  def end_time
    LotteryTime.set_end_time(self.lottery_times, self.lottery_dates)
  end

  def start_time
    LotteryTime.set_start_time(self.lottery_times, self.lottery_dates)
  end
end
