class Member < TableLess
  column :name, :string
  column :email, :string
  column :password, :string
  column :password_confirmation, :string
  column :role, :string
  column :parent_id, :string
end
