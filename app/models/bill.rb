class Bill < ApplicationRecord
  has_many :lotteries, dependent: :delete_all
  belongs_to :user
  belongs_to :invoice, optional: true

  STATUS = {"draft" => 'รอยืนยัน', "confirmed" => 'ยืนยันแล้ว', "active" => 'ถูกรางวัล', "passive" => 'ไม่ถูกรางวัล'}
  LABEL = {"draft" => 'warning', "confirmed" => 'success', "active" => 'success', "passive" => 'danger'}

  # ROUND = 

  # def self.create_bill_round(time)
  #   hash = {}
  #   times.each { |i| hash[i] = 'free' }
  #   hash
  # end
end
