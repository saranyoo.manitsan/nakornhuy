class LotteryTime < ApplicationRecord
  belongs_to :lottery_topic, optional: true

  def self.set_date_time(times, dates)

    times.each do |time|
      if DateTime.now.between?(Time.zone.parse(time.start_time), Time.zone.parse(time.end_time)) && (dates.pluck(:day).include?('everyday') || dates.pluck(:day).include?(Date.today.strftime("%A").downcase))
        return time.round_name
      end
    end
  end

  def self.set_round(times, dates)

    times.each_with_index do |time, index|
      start_time = Time.zone.parse(time.start_time)
      end_time = Time.zone.parse(time.end_time)

      if DateTime.now.between?(start_time, end_time) && (dates.pluck(:day).include?('everyday') || dates.pluck(:day).include?(Date.today.strftime("%A").downcase))
        return index+1
      end
    end
  end

  def self.set_admin_round(times, dates)

    times.each_with_index do |time, index|
      start_time = Time.zone.parse(time.start_time)
      admin_end_time = Time.zone.parse(time.admin_end_time)
      admin_end_time = Time.zone.parse(time.admin_end_time).tomorrow if start_time > admin_end_time

      if DateTime.now.between?(start_time, admin_end_time) && (dates.pluck(:day).include?('everyday') || dates.pluck(:day).include?(Date.today.strftime("%A").downcase))
        return index+1
      else
        return 0
      end
    end
  end

  def self.set_admin_date_time(times, dates)

    times.each do |time|
      start_time = Time.zone.parse(time.start_time)
      admin_end_time = Time.zone.parse(time.admin_end_time)
      admin_end_time = Time.zone.parse(time.admin_end_time).tomorrow if start_time > admin_end_time
      if DateTime.now.between?(start_time, admin_end_time) && (dates.pluck(:day).include?('everyday') || dates.pluck(:day).include?(Date.today.strftime("%A").downcase))
        return time.round_name
      end
    end
  end

  def self.create_round_hash(times)
    t_hash = {}
    times.each_with_index do |time, index|
      t_hash[time.round_name] = index+1
    end
    t_hash
  end

  def self.set_lot_round(title, round)
    lt = LotteryTopic.find_by(title: title)
    lt.lottery_times.pluck(:round_name)[round.to_i-1]
  end

  def self.set_end_time(times, dates)
    times.each do |time|
      if DateTime.now.between?(Time.zone.parse(time.start_time), Time.zone.parse(time.end_time)) && (dates.pluck(:day).include?('everyday') || dates.pluck(:day).include?(Date.today.strftime("%A").downcase))
        return Time.zone.parse(time.end_time)
        # return Time.zone.parse(time.end_time).strftime("%Y, %-m , %-d , %H , %M, %S")
      end
    end
  end

  def self.set_start_time(times, dates)
    times.each do |time|
      unless DateTime.now.between?(Time.zone.parse(time.start_time), Time.zone.parse(time.end_time)) && (dates.pluck(:day).include?('everyday') || dates.pluck(:day).include?(Date.today.strftime("%A").downcase))
        if Time.zone.parse(time.start_time) < DateTime.now
          return Time.zone.parse(time.start_time).tomorrow
        else
          return Time.zone.parse(time.start_time)
        end
      end
    end
  end

end
