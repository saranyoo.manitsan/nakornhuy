class Lottery < ApplicationRecord

  belongs_to :user
  belongs_to :bill

  POSITION = { บน: 1, ล่าง: 2, บนล่าง: 3}
  THREE_DIGITS_POSITION = { ตรง: 4, โต้ด: 5}
  LOTTERRIES = LotteryTopic.pluck(:title)
  LOTTERRY_OPTION = {วิน: 'vin', วินไม่เบิ้ล: 'vin_no_double', รูด: 'rood', รูดไม่เบิ้ล: 'rood_no_double'}
  ROUND = {"1" => 'เปิดเช้า', "2" => 'ปิดเที่ยง', "3" => 'เปิดบ่าย', "4" => 'ปิดเย็น'}
  LOTTERYPOSITION = {"1" => 'บน', "2" => 'ล่าง', "4" => 'ตรง', "5" => 'โต้ด'}

  def self.rood(num, external="")
    # data = num.split(/\s|'|,|-/)
    data = num.split("")
    data.delete(" ")
    # .flatten

    arr = []

    data.each do |nn|

      new_array = []
      new_array_reverse = []
      array = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
      array = data if (external == "vin") || (external =='vin_no_double')
      array.each { |x| new_array.push(nn+x) }
      array.each { |x| new_array_reverse.push(x+nn) }

      array = new_array+new_array_reverse
      array.delete(nn+nn) if external == "rood_no_double" || external == "vin_no_double"
      arr.push(array.uniq)
    end

    arr = arr.sum
    arr = arr.uniq if external == "vin_no_double" || external == "vin"
    arr.delete("")
    arr
  end

  def self.reverse(num, reverse)
    data = num.split(/\s|'|\/|,|-/)

    arr = []
    data.each do |nn|

      arr.push(nn)
      if reverse == "1"
        arr.push(nn.reverse) if (nn.split("")[0].to_i - nn.split("")[1].to_i) != 0
      end

    end
    arr.delete("")
    arr
  end

  def self.run_array(num)
    data = num.split("")

    arr = []
    data.each do |nn|
      arr.push(nn)
    end

    arr.uniq
  end

  def self.three_to_six(num, six_reverse)
    data = num.split(/\s|'|\/|,|-/)

    arr = []

    if six_reverse == "1"
      data.each do |nn|
        # arr.push(nn)
        new_arr = nn.split(//)
        new_arr.each_with_index do |pp, ii|
          # byebug
          dd = new_arr.delete_at(0)
          new_arr.push(dd)

          arr.push(new_arr.sum)
          arr.push(new_arr.sum.reverse)


        end
      end
    else
      data.each do |nn|
        arr.push(nn)
      end
    end
    arr.delete("")
    arr = arr.uniq
    arr
  end

  def self.create_html(array)
    html = ""
    lock = false
    array.each do |nn|
      if (nn.length !=2 || !is_number?(nn)) &&  nn != "00"
        lock = true
      end
      html.insert(-1, "<div class='label label-info'>"+nn+"</div> ") if nn.length ==2 && is_number?(nn)
      html.insert(-1, "<div class='label label-danger'>"+nn+"</div> ") if nn.length !=2 || !is_number?(nn)
    end
    return {"html": "<p class='col-xs-12'>" + html +"</p>", "lock": lock} 
  end

  def self.create_three_html(array)
    html = ""
    lock = false

    array.each do |nn|
      if nn.length !=3 || !is_number?(nn)
        lock = true
      end
      html.insert(-1, "<div class='label label-info'>"+nn+"</div> ") if nn.length ==3 && is_number?(nn)
      html.insert(-1, "<div class='label label-danger'>"+nn+"</div> ") if nn.length !=3 || !is_number?(nn)
    end
    return {"html": "<p class='col-xs-12'>" + html +"</p>", "lock": lock}
  end

  def self.create_run_html(array)
    html = ""
    lock = false

    array.each do |nn|
      if nn.length !=1 || !is_number?(nn)
        lock = true
      end
      html.insert(-1, "<div class='label label-info'>"+nn+"</div> ") if nn.length ==1 && is_number?(nn)
      html.insert(-1, "<div class='label label-danger'>"+nn+"</div> ") if nn.length !=1 || !is_number?(nn)
    end
    return {"html": "<p class='col-xs-12'>" + html +"</p>", "lock": lock}
  end

  def self.rood_two_digits(round, params, user, bill)
    if params[:lottery][:bottom_price].present?
      rood_bottom(round, params, user, bill, "2")
    end
    if params[:lottery][:price].present?
      rood_top(round, params, user, bill, "1")
    end
  end

  def self.rood_top(round, params, user, bill, position)
    data = rood(params[:lottery][:datum], params[:lottery][:option])
    fight_value = (((params[:lottery][:price]).to_f*user.user_datum.fight_percent.to_i).to_f/100)
    price = params[:lottery][:price].to_f-fight_value
    two_fight_limit = user.user_datum.two_fight_limit.to_i
    fight_summary = fight_value > two_fight_limit ? two_fight_limit : fight_value
    data.each { |x| 
      l = Lottery.create(
        datum: x, 
        position: position, 
        price: price,
        real_price: params[:lottery][:price].to_f,
        round: round,
        user: user,
        bill: bill,
        won_amount: price.to_f*user.user_datum.percent.to_i,
        discount: (price.to_f*user.user_datum.user_percent.to_i).to_f/100,
        group: bill.group,
        source: data,
        fight_percent: user.user_datum.fight_percent.to_i,
        fight_amount: fight_summary,
        fight_paid: user.user_datum.percent*fight_summary,
        result_amount: params[:lottery][:price].to_f*user.user_datum.percent.to_i
      )

      l.update_bill

    }
  end

  def self.rood_bottom(round, params, user, bill, position)
    data = rood(params[:lottery][:datum], params[:lottery][:option])
    fight_value = (((params[:lottery][:bottom_price]).to_f*user.user_datum.fight_percent.to_i).to_f/100)
    price = params[:lottery][:bottom_price].to_f-fight_value
    two_fight_limit = user.user_datum.two_fight_limit.to_i
    fight_summary = fight_value > two_fight_limit ? two_fight_limit : fight_value
    data.each { |x| 
      l = Lottery.create(
        datum: x, 
        position: position, 
        price: price,
        real_price: params[:lottery][:bottom_price].to_f,
        round: round,
        user: user,
        bill: bill,
        won_amount: price.to_f*user.user_datum.percent.to_i,
        discount: (price.to_f*user.user_datum.user_percent.to_i).to_f/100,
        group: bill.group,
        source: data,
        fight_percent: user.user_datum.fight_percent.to_i,
        fight_amount: fight_summary,
        fight_paid: user.user_datum.percent*fight_summary,
        result_amount: params[:lottery][:bottom_price].to_f*user.user_datum.percent.to_i
      )

      l.update_bill

    }
  end

  def self.two_digits(round, params, user, bill, reverse)
    if params[:lottery][:bottom_price].present?
      two_digits_bottom(round, params, user, bill, "2", reverse)
    end
    if params[:lottery][:price].present?
      two_digits_top(round, params, user, bill, "1", reverse)
    end
  end

  def self.two_digits_top(round, params, user, bill, position, reverse)
    data = reverse(params[:lottery][:datum], reverse)
    fight_value = (((params[:lottery][:price]).to_f*user.user_datum.fight_percent.to_i).to_f/100)
    price = params[:lottery][:price].to_f-fight_value
    two_fight_limit = user.user_datum.two_fight_limit.to_i
    fight_summary = fight_value > two_fight_limit ? two_fight_limit : fight_value
    data.each { |x| 
      l = Lottery.create(
        datum: x, 
        position: position, 
        price: price,
        real_price: params[:lottery][:price].to_f,
        round: round,
        user: user,
        bill: bill,
        won_amount: price.to_f*user.user_datum.percent.to_i,
        discount: (price.to_f*user.user_datum.user_percent.to_i).to_f/100,
        group: bill.group,
        source: data,
        fight_percent: user.user_datum.fight_percent.to_i,
        fight_amount: fight_summary,
        fight_paid: user.user_datum.percent*fight_summary,
        result_amount: params[:lottery][:price].to_f*user.user_datum.percent.to_i
      )


      l.update_bill

    }
  end

  def self.two_digits_bottom(round, params, user, bill, position, reverse)
    data = reverse(params[:lottery][:datum], reverse)
    fight_value = (((params[:lottery][:bottom_price]).to_f*user.user_datum.fight_percent.to_i).to_f/100)
    price = params[:lottery][:bottom_price].to_f-fight_value
    two_fight_limit = user.user_datum.two_fight_limit.to_i
    fight_summary = fight_value > two_fight_limit ? two_fight_limit : fight_value
    data.each_with_index { |x, i| 
      l = Lottery.create(
        datum: x, 
        position: position, 
        price: price,
        real_price: params[:lottery][:bottom_price].to_f,
        round: round,
        user: user,
        bill: bill,
        won_amount: price.to_f*user.user_datum.percent.to_i,
        discount: (price.to_f*user.user_datum.user_percent.to_i).to_f/100,
        group: bill.group,
        source: data,
        fight_percent: user.user_datum.fight_percent.to_i,
        fight_amount: fight_summary,
        fight_paid: user.user_datum.percent*fight_summary,
        result_amount: params[:lottery][:bottom_price].to_f*user.user_datum.percent.to_i
      )

      l.update_bill

    }
  end

  def self.run(round, params, user, bill)
    if params[:lottery][:bottom_price].present?
      run_bottom(round, params, user, bill, "2")
    end
    if params[:lottery][:price].present?
      run_top(round, params, user, bill, "1")
    end
  end

  def self.run_top(round, params, user, bill, position)
    data = run_array(params[:lottery][:datum])
    fight_value = (((params[:lottery][:price]).to_f*user.user_datum.fight_run.to_i).to_f/100)
    price = params[:lottery][:price].to_f-fight_value
    run_fight_limit = user.user_datum.run_fight_limit.to_i
    fight_summary = fight_value > run_fight_limit ? run_fight_limit : fight_value
    data.each { |x| 
      l = Lottery.create(
        datum: x, 
        position: position, 
        price: price,
        real_price: params[:lottery][:price].to_f,
        round: round,
        user: user,
        bill: bill,
        won_amount: price.to_f*user.user_datum.run_percent.to_i,
        discount: (price.to_f*user.user_datum.run_user_percent.to_i).to_f/100,
        group: bill.group,
        source: data,
        fight_percent: user.user_datum.fight_run.to_i,
        fight_amount: fight_summary,
        fight_paid: user.user_datum.run_percent*fight_summary,
        result_amount: params[:lottery][:price].to_f*user.user_datum.run_percent.to_i
      )

      l.update_bill

    }
  end

  def self.run_bottom(round, params, user, bill, position)
    data = run_array(params[:lottery][:datum])
    fight_value = (((params[:lottery][:bottom_price]).to_f*user.user_datum.fight_run.to_i).to_f/100)
    price = params[:lottery][:bottom_price].to_f-fight_value
    run_fight_limit = user.user_datum.run_fight_limit.to_i
    fight_summary = fight_value > run_fight_limit ? run_fight_limit : fight_value
    data.each { |x| 
      l = Lottery.create(
        datum: x, 
        position: position, 
        price: price,
        real_price: params[:lottery][:bottom_price].to_f,
        round: round,
        user: user,
        bill: bill,
        won_amount: price.to_f*user.user_datum.run_bottom_percent.to_i,
        discount: (price.to_f*user.user_datum.run_user_percent.to_i).to_f/100,
        group: bill.group,
        source: data,
        fight_percent: user.user_datum.fight_run.to_i,
        fight_amount: fight_summary,
        fight_paid: user.user_datum.run_bottom_percent*fight_summary,
        result_amount: params[:lottery][:bottom_price].to_f*user.user_datum.run_bottom_percent.to_i
      )

      l.update_bill

    }
  end
  def self.three(round, params, user, bill, six_reverse)
    if params[:lottery][:price].present?
      data = three_to_six(params[:lottery][:datum], six_reverse)
      fight_value = (((params[:lottery][:price]).to_f*user.user_datum.fight_three.to_i).to_f/100)
      price = params[:lottery][:price].to_f-fight_value
      three_fight_limit = user.user_datum.three_fight_limit.to_i
      fight_summary = fight_value > three_fight_limit ? three_fight_limit : fight_value
      data.each { |x|
        l = Lottery.create(
          datum: x, 
          position: 4,
          real_price: params[:lottery][:price].to_f,
          price: price,
          round: round,
          user: user,
          bill: bill,
          won_amount: price.to_f*user.user_datum.three_percent.to_i,
          discount: (price.to_f*user.user_datum.three_user_percent.to_i).to_f/100,
          group: bill.group,
          source: data,
          fight_percent: user.user_datum.fight_three.to_i,
          fight_amount: fight_summary,
          fight_paid: user.user_datum.three_percent*fight_summary,
          result_amount: params[:lottery][:price].to_f*user.user_datum.three_percent.to_i
        )

        l.update_bill

      }
    end


    three_all_reverse(round, params, user, bill, six_reverse) if params[:lottery][:reverse_price].present?
  end

  def self.three_all_reverse(round, params, user, bill, six_reverse)
    data = three_to_six(params[:lottery][:datum], six_reverse)
    fight_value = (((params[:lottery][:reverse_price]).to_i*user.user_datum.fight_three.to_i).to_f/100)
    price = params[:lottery][:reverse_price].to_i-fight_value
    three_fight_limit = user.user_datum.three_fight_limit.to_i
    fight_summary = fight_value > three_fight_limit ? three_fight_limit : fight_value
    data.each { |x| 
      l = Lottery.create(
        datum: x, 
        position: 5,
        real_price: params[:lottery][:reverse_price].to_f,
        price: price,
        round: round,
        user: user,
        bill: bill,
        won_amount: price.to_f*user.user_datum.three_reverse_percent.to_i,
        discount: (price.to_f*user.user_datum.three_user_percent.to_i).to_f/100,
        group: bill.group,
        source: data,
        fight_percent: user.user_datum.fight_three.to_i,
        fight_amount: fight_summary,
        fight_paid: user.user_datum.three_reverse_percent*fight_summary,
        result_amount: params[:lottery][:reverse_price].to_i*user.user_datum.three_reverse_percent.to_i
      )
      
      l.update_bill

    }
  end

  def self.is_number?(string)
    true if Float(string) && Float(string) >= 0 rescue false
  end

  def update_bill
    bill = self.bill
    bill.update_attributes(
      total: bill.total.to_f+self.price.to_f,
      discount: sprintf('%.1f', bill.discount.to_f+self.discount.to_f),
      real_price: bill.real_price.to_f+self.real_price.to_f,
      fight_amount: bill.fight_amount.to_f+self.fight_amount.to_f,
    )
  end

end
