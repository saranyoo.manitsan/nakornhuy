class Post < ApplicationRecord
  LIST = { เปิดเช้า: 1, ปิดเที่ยง: 2, เปิดบ่าย: 3, ปิดเย็น: 4 }

  LOTTERY_LIST = { setthaivip: 'setthaivip' }

  validates :three, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }
  validates_length_of :three, is: 1,  message: "สามารถใส่ตัวเลขได้เพียง 1 หลัก"
  validates_numericality_of :three, message: "ใส่ได้เพียงตัวเลข"

  validates :top, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }
  validates_length_of :top, is: 2,  message: "สามารถใส่ตัวเลขได้เพียง 2 หลัก"
  validates_numericality_of :top, message: "ใส่ได้เพียงตัวเลข"

  validates :bottom, presence: { message: "ไม่สามารถใส่เป็นค่าว่างได้" }
  validates_length_of :bottom, is: 2,  message: "สามารถใส่ตัวเลขได้เพียง 2 หลัก"
  validates_numericality_of :bottom, message: "ใส่ได้เพียงตัวเลข"

  def self.lottery_analyze(round, lot)
    transaction do
      Lottery.joins(:bill).where(:bills => {lottery_id: lot, original_id: nil, :status => 'confirmed', round: round, created_at: Date.today.all_day })
    end
  end
end
