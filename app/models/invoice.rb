class Invoice < ApplicationRecord
  belongs_to :user
  has_many :bills

  STATUS = {"wait" => 'รอชำระ', "paid" => 'ชำระแล้ว'}
end
