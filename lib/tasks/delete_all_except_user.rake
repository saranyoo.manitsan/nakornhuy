namespace :remove do
  task :all => :environment do
    Post.delete_all
    Invoice.delete_all
    Bill.delete_all
    Lottery.delete_all
  end
end
