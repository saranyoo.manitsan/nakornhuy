# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_07_17_180842) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bills", force: :cascade do |t|
    t.string "lottery_id"
    t.integer "user_id"
    t.string "status"
    t.string "round"
    t.string "total"
    t.string "amount_paid"
    t.string "amount_total"
    t.string "amount_won"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.integer "invoice_id"
    t.integer "two_top_amount_won"
    t.integer "two_top_amount"
    t.integer "two_bottom_amount_won"
    t.integer "two_bottom_amount"
    t.integer "three_amount_won"
    t.integer "three_amount"
    t.integer "three_reverse_amount_won"
    t.integer "three_reverse_amount"
    t.integer "run_top_amount_won"
    t.integer "run_top_amount"
    t.integer "run_bottom_amount_won"
    t.integer "run_bottom_amount"
    t.integer "owner_id"
    t.integer "original_id"
    t.float "discount"
    t.integer "group", default: 0
    t.float "fight_amount", default: 0.0
    t.float "fight_percent", default: 0.0
    t.float "fight_paid", default: 0.0
    t.float "real_price", default: 0.0
    t.float "result_amount", default: 0.0
  end

  create_table "invoices", force: :cascade do |t|
    t.integer "user_id"
    t.string "round"
    t.string "status"
    t.string "total"
    t.string "amount_paid"
    t.string "amount_total"
    t.string "amount_won"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "two_top_amount_won"
    t.integer "two_top_amount"
    t.integer "two_bottom_amount_won"
    t.integer "two_bottom_amount"
    t.integer "three_amount_won"
    t.integer "three_amount"
    t.integer "three_reverse_amount_won"
    t.integer "three_reverse_amount"
    t.integer "run_top_amount_won"
    t.integer "run_top_amount"
    t.integer "run_bottom_amount_won"
    t.integer "run_bottom_amount"
    t.float "discount"
    t.float "fight_amount", default: 0.0
    t.float "fight_percent", default: 0.0
    t.float "fight_paid", default: 0.0
    t.float "real_price", default: 0.0
    t.float "result_amount", default: 0.0
    t.string "lottery_id"
  end

  create_table "lotteries", force: :cascade do |t|
    t.string "datum"
    t.string "position"
    t.string "price"
    t.string "round"
    t.string "status"
    t.string "name"
    t.integer "user_id"
    t.integer "lot_id"
    t.integer "bill_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "won", default: false
    t.integer "won_amount"
    t.float "discount"
    t.string "group"
    t.string "source", default: [], array: true
    t.float "fight_amount", default: 0.0
    t.float "fight_percent", default: 0.0
    t.float "fight_paid", default: 0.0
    t.float "real_price", default: 0.0
    t.float "result_amount", default: 0.0
    t.string "lottery_id"
  end

  create_table "lottery_dates", force: :cascade do |t|
    t.string "day"
    t.integer "lottery_topic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lottery_times", force: :cascade do |t|
    t.string "start_time"
    t.string "end_time"
    t.integer "lottery_topic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "round_name"
    t.string "admin_end_time"
  end

  create_table "lottery_topics", force: :cascade do |t|
    t.string "name"
    t.string "title"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "avatar"
    t.boolean "close", default: false
  end

  create_table "posts", force: :cascade do |t|
    t.string "top"
    t.string "bottom"
    t.string "three"
    t.string "status"
    t.string "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "lottery_id"
  end

  create_table "summaries", force: :cascade do |t|
    t.string "datum"
    t.string "position"
    t.integer "total_purchaser"
    t.integer "number_purchaser"
    t.integer "total_loss"
    t.string "round"
    t.string "lottery_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_data", force: :cascade do |t|
    t.integer "percent"
    t.integer "user_percent"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "three_percent"
    t.integer "three_reverse_percent"
    t.integer "run_percent"
    t.boolean "fight", default: false
    t.integer "fight_percent", default: 0
    t.integer "run_bottom_percent"
    t.integer "three_user_percent"
    t.integer "run_user_percent"
    t.integer "credit", default: 0
    t.integer "fightable"
    t.integer "fight_three", default: 0
    t.integer "fight_run", default: 0
    t.integer "two_fight_limit", default: 1000
    t.integer "three_fight_limit", default: 1000
    t.integer "run_fight_limit", default: 1000
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "name"
    t.string "role"
    t.string "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "username"
    t.text "lottery_owner", default: [], array: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
