# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


admin = User.new
admin.username = "admin1"
admin.name = "admin"
admin.role = "admin"
admin.email = "admin@admin.com"
admin.password = "asdqwe123"
admin.password_confirmation = "asdqwe123"
admin.save

agent = User.new
agent.username = "agent1"
agent.name = "agent"
agent.role = "agent"
agent.email = "agent@agent.com"
agent.parent_id = "#{admin.id}"
agent.password = "asdqwe123"
agent.password_confirmation = "asdqwe123"
agent.save

member = User.new
member.username = "member1"
member.name = "member"
member.role = "member"
member.email = "member@member.com"
member.parent_id = "#{agent.id}"
member.password = "asdqwe123"
member.password_confirmation = "asdqwe123"
member.save

dajim = User.new
dajim.username = "dajim"
dajim.name = "dajim"
dajim.role = "member"
dajim.email = "dajim@member.com"
dajim.parent_id = "#{agent.id}"
dajim.password = "asdqwe123"
dajim.password_confirmation = "asdqwe123"
dajim.save


UserDatum.create(user: admin, percent: 96, user_percent: 8, three_percent: 500, three_reverse_percent: 125, run_percent: 3, run_bottom_percent: 4)
UserDatum.create(user: agent, percent: 96, user_percent: 8, three_percent: 500, three_reverse_percent: 125, run_percent: 3, run_bottom_percent: 4)
UserDatum.create(user: member, percent: 90, user_percent: 8, three_percent: 500, three_reverse_percent: 125, run_percent: 3, run_bottom_percent: 4)
UserDatum.create(user: dajim, percent: 90, user_percent: 8, three_percent: 500, three_reverse_percent: 125, run_percent: 3, run_bottom_percent: 4)
