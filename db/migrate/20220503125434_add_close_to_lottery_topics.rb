class AddCloseToLotteryTopics < ActiveRecord::Migration[5.2]
  def change
    add_column :lottery_topics, :close, :boolean, default: false
    add_column :lottery_times, :round_name, :string
  end
end
