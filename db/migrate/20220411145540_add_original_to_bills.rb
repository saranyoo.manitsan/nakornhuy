class AddOriginalToBills < ActiveRecord::Migration[5.2]
  def change
    add_column :bills, :original_id, :integer
  end
end
