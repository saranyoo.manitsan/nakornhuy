class AddDiscountToLotteries < ActiveRecord::Migration[5.2]
  def change
    add_column :lotteries, :discount, :float
  end
end
