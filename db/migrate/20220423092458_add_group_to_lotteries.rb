class AddGroupToLotteries < ActiveRecord::Migration[5.2]
  def change
    add_column :lotteries, :group, :string
  end
end
