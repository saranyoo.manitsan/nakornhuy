class AddInvoiceIdToBill < ActiveRecord::Migration[5.2]
  def change
    add_column :bills, :invoice_id, :integer
  end
end
