class AddOwnerToBills < ActiveRecord::Migration[5.2]
  def change
    add_column :bills, :owner_id, :integer
  end
end
