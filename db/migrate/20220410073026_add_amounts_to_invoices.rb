class AddAmountsToInvoices < ActiveRecord::Migration[5.2]
  def change
    add_column :invoices, :two_top_amount_won, :integer
    add_column :invoices, :two_top_amount, :integer
    add_column :invoices, :two_bottom_amount_won, :integer
    add_column :invoices, :two_bottom_amount, :integer
    add_column :invoices, :three_amount_won, :integer
    add_column :invoices, :three_amount, :integer
    add_column :invoices, :three_reverse_amount_won, :integer
    add_column :invoices, :three_reverse_amount, :integer
    add_column :invoices, :run_top_amount_won, :integer
    add_column :invoices, :run_top_amount, :integer
    add_column :invoices, :run_bottom_amount_won, :integer
    add_column :invoices, :run_bottom_amount, :integer
  end
end
