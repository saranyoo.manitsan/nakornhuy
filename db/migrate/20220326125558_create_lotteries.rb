class CreateLotteries < ActiveRecord::Migration[5.2]
  def change
    create_table :lotteries do |t|
      t.string :datum
      t.string :position
      t.string :price
      t.string :round
      t.string :status
      t.string :name
      t.integer :user_id
      t.integer :lot_id
      t.integer :bill_id

      t.timestamps
    end
  end
end
