class CreateUserData < ActiveRecord::Migration[5.2]
  def change
    create_table :user_data do |t|
      t.integer :percent
      t.integer :user_percent
      t.integer :user_id

      t.timestamps
    end
  end
end
