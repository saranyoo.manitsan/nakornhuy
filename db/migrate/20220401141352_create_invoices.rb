class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.integer :user_id
      t.string :round
      t.string :status
      t.string :total
      t.string :amount_paid
      t.string :amount_total
      t.string :amount_won

      t.timestamps
    end
  end
end
