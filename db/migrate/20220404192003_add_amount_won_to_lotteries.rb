class AddAmountWonToLotteries < ActiveRecord::Migration[5.2]
  def change
    add_column :lotteries, :won, :boolean, default: false
    add_column :lotteries, :won_amount, :integer
  end
end
