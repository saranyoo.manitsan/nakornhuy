class CreateLotteryDates < ActiveRecord::Migration[5.2]
  def change
    create_table :lottery_dates do |t|
      t.string :day
      t.integer :lottery_topic_id

      t.timestamps
    end
  end
end
