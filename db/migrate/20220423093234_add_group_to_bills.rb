class AddGroupToBills < ActiveRecord::Migration[5.2]
  def change
    add_column :bills, :group, :integer, default: 0
  end
end
