class ChangeBillIntToFloat < ActiveRecord::Migration[5.2]
  def change
    change_column(:bills, :fight_amount, :float)
    change_column(:bills, :fight_percent, :float)
    change_column(:bills, :fight_paid, :float)
    change_column(:bills, :real_price, :float)
    change_column(:bills, :result_amount, :float)
  end
end
