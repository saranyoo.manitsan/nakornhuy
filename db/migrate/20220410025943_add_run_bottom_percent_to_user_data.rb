class AddRunBottomPercentToUserData < ActiveRecord::Migration[5.2]
  def change
    add_column :user_data, :run_bottom_percent, :integer
  end
end
