class AddNameToBill < ActiveRecord::Migration[5.2]
  def change
    add_column :bills, :name, :string
  end
end
