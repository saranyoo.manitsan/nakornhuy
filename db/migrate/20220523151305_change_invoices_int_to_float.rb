class ChangeInvoicesIntToFloat < ActiveRecord::Migration[5.2]
  def change
    change_column(:invoices, :fight_amount, :float)
    change_column(:invoices, :fight_percent, :float)
    change_column(:invoices, :fight_paid, :float)
    change_column(:invoices, :real_price, :float)
    change_column(:invoices, :result_amount, :float)
  end
end
