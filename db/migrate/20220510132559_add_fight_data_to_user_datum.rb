class AddFightDataToUserDatum < ActiveRecord::Migration[5.2]
  def change
    add_column :user_data, :fight_three, :integer, default: 0
    add_column :user_data, :fight_run, :integer, default: 0
    add_column :user_data, :two_fight_limit, :integer, default: 1000
    add_column :user_data, :three_fight_limit, :integer, default: 1000
    add_column :user_data, :run_fight_limit, :integer, default: 1000
  end
end
