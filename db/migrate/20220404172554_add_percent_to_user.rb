class AddPercentToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :user_data, :three_percent, :integer
    add_column :user_data, :three_reverse_percent, :integer
    add_column :user_data, :run_percent, :integer
    add_column :user_data, :fight, :boolean, default: false
    add_column :user_data, :fight_percent, :integer
  end
end
