class AddLotteryOwner < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :lottery_owner, :text, array: true, default: []
  end
end
