class AddAmountsToBills < ActiveRecord::Migration[5.2]
  def change
    add_column :bills, :two_top_amount_won, :integer
    add_column :bills, :two_top_amount, :integer
    add_column :bills, :two_bottom_amount_won, :integer
    add_column :bills, :two_bottom_amount, :integer
    add_column :bills, :three_amount_won, :integer
    add_column :bills, :three_amount, :integer
    add_column :bills, :three_reverse_amount_won, :integer
    add_column :bills, :three_reverse_amount, :integer
    add_column :bills, :run_top_amount_won, :integer
    add_column :bills, :run_top_amount, :integer
    add_column :bills, :run_bottom_amount_won, :integer
    add_column :bills, :run_bottom_amount, :integer
  end
end
