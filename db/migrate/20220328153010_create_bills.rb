class CreateBills < ActiveRecord::Migration[5.2]
  def change
    create_table :bills do |t|
      t.string :lottery_id
      t.integer :user_id
      t.string :status
      t.string :round
      t.string :total
      t.string :amount_paid
      t.string :amount_total
      t.string :amount_won

      t.timestamps
    end
  end
end
