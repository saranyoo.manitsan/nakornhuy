class AddLotteryIdToLotteries < ActiveRecord::Migration[5.2]
  def change
    add_column :lotteries, :lottery_id, :string
    add_column :invoices, :lottery_id, :string
  end
end
