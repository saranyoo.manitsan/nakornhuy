class AddSourceToLotteries < ActiveRecord::Migration[5.2]
  def change
    add_column :lotteries, :source, :string, array: true, default: []
  end
end
