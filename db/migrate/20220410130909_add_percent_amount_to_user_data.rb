class AddPercentAmountToUserData < ActiveRecord::Migration[5.2]
  def change
    add_column :user_data, :three_user_percent, :integer
    add_column :user_data, :run_user_percent, :integer
  end
end
