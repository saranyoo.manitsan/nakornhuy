class CreateLotteryTimes < ActiveRecord::Migration[5.2]
  def change
    create_table :lottery_times do |t|
      t.string :start_time
      t.string :end_time
      t.integer :lottery_topic_id

      t.timestamps
    end
  end
end
