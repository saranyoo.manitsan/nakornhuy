class CreateSummaries < ActiveRecord::Migration[5.2]
  def change
    create_table :summaries do |t|
      t.string :datum
      t.string :position
      t.integer :total_purchaser
      t.integer :number_purchaser
      t.integer :total_loss
      t.string :round
      t.string :lottery_name

      t.timestamps
    end
  end
end
