class AddCreditToUserDatum < ActiveRecord::Migration[5.2]
  def change
    add_column :user_data, :credit, :integer, default: 0
  end
end
