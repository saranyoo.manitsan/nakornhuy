class ChangeIntToFloat < ActiveRecord::Migration[5.2]
  def change
    change_column(:lotteries, :fight_amount, :float)
    change_column(:lotteries, :fight_percent, :float)
    change_column(:lotteries, :fight_paid, :float)
    change_column(:lotteries, :real_price, :float)
    change_column(:lotteries, :result_amount, :float)
  end
end
