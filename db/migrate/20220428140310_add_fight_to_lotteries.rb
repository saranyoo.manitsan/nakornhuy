class AddFightToLotteries < ActiveRecord::Migration[5.2]
  def change
    add_column :lotteries, :fight_amount, :integer, default: 0
    add_column :lotteries, :fight_percent, :integer, default: 0
    add_column :lotteries, :fight_paid, :integer, default: 0
    add_column :lotteries, :real_price, :integer, default: 0
    add_column :lotteries, :result_amount, :integer, default: 0
    add_column :bills, :fight_amount, :integer, default: 0
    add_column :bills, :fight_percent, :integer, default: 0
    add_column :bills, :fight_paid, :integer, default: 0
    add_column :bills, :real_price, :integer, default: 0
    add_column :bills, :result_amount, :integer, default: 0

    add_column :invoices, :fight_amount, :integer, default: 0
    add_column :invoices, :fight_percent, :integer, default: 0
    add_column :invoices, :fight_paid, :integer, default: 0
    add_column :invoices, :real_price, :integer, default: 0
    add_column :invoices, :result_amount, :integer, default: 0
  end
end
