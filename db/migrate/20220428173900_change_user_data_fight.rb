class ChangeUserDataFight < ActiveRecord::Migration[5.2]
  def change
    change_column :user_data, :fight_percent, :integer, default: 0
  end
end
