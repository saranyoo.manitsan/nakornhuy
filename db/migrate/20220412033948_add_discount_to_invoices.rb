class AddDiscountToInvoices < ActiveRecord::Migration[5.2]
  def change
    add_column :invoices, :discount, :float
  end
end
