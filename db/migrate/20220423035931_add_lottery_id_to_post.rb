class AddLotteryIdToPost < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :lottery_id, :string
  end
end
