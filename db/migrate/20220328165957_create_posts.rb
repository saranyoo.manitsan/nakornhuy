class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :top
      t.string :bottom
      t.string :three
      t.string :status
      t.string :note

      t.timestamps
    end
  end
end
