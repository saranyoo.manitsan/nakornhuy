class AddDiscountToBills < ActiveRecord::Migration[5.2]
  def change
    add_column :bills, :discount, :float
  end
end
