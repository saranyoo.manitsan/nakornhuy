Rails.application.routes.draw do
  resources :lottery_topics
  resources :posts
  resource :users,
    only: [:edit, :update, :delete],
    controller: 'devise/registrations',
    as: :user_registration do
    get 'cancel'
  end

  devise_for :users, skip: [:registrations]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "home#index"
  resources :users do
    resource :reset_passwords
  end
  resources :admins
  resources :admins,
    only: [:edit, :update, :delete],
    controller: 'devise/registrations',
    as: :admin_registration do
    get 'cancel'
  end
  resources :agents do 
    resources :member_invoices do
      resources :members do
        get '/confirmed', to: 'member_invoices#confirmed'
      end
    end
  end
  resources :members
  resources :profiles do
    resource :passwords
  end
  resources :lotteries
  resources :bills do
    resources :lotteries
    get '/confirmed', to: 'bills#confirmed'
  end

  resources :invoices

  get 'rood', to: 'lotteries#rood'
  get 'reverse', to: 'lotteries#reverse'
  get 'all_reverse', to: 'lotteries#all_reverse'
  get 'run', to: 'lotteries#run'
  get 'destroy_group', to: 'lotteries#destroy_group'

  get '/create_lottery', to: 'home#owner_lottery'
end
